package com.htc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.client.EmployeeClient;
import com.htc.dto.EmployeeResponse;

@Service
public class BillService {
	@Autowired
	private EmployeeClient employeeClient;
	
	public EmployeeResponse findById(long id) {
		return employeeClient.findById(id);
	}
}
