package com.htc.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.client.EmployeeClient;
import com.htc.dto.EmployeeResponse;
import com.htc.service.BillService;

@RestController
@RequestMapping("/bill")
public class BillApi {
	@Autowired 
	private BillService billService;
	
	@GetMapping("/{id}")
	public EmployeeResponse findById(@PathVariable long id){
		return billService.findById(id);
	}
}