package com.htc.client;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.dto.EmployeeResponse;

@FeignClient(name="employee-microservice")
@RibbonClient(name="employee-microservice")
public interface EmployeeClient {
	@GetMapping("/employee/{id}")
	public EmployeeResponse findById(@PathVariable("id") long id);
}
